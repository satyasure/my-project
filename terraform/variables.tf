variable "region" {
  description = "The aws region to create resources"
  default     = "ap-southeast-1"
}

variable "aws_access_key" {
  type        = string
  description = "the aws access key"
  sensitive   = true
}

variable "aws_secret_key" {
  type        = string
  description = "the aws secret key"
  sensitive   = true
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "to enable dns hostnames"
  default     = true
}

variable "vpc_cidr_block" {
  type        = string
  description = "cidr range for vpc"
  default     = "10.0.0.0/16"
}

variable "vpc_subnet1_cidr_block" {
  type        = string
  description = "cidr range for subnet1 in vpc"
  default     = "10.0.0.0/24"
}

variable "map_public_ip_on_launch" {
  type        = bool
  description = "get public ip on launch"
  default     = true
}

variable "instance_type" {
  type        = string
  description = "Type for EC2 Instnace"
  default     = "t2.micro"
}

variable "company" {
  type        = string
  description = "company name"
  default     = "govtech"
}

variable "project" {
  type        = string
  description = "Project name for resource tagging"
  default     = "IN&DC"
}
